`npm install` &
`npm run start`

I couldn't run with proxy, I was getting some error from CloudFlare, so I used the CORS extension for chrome https://mybrowseraddon.com/access-control-allow-origin.html

You can check a demo here
[Demo video](demo.mov)
