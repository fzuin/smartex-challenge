import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HeaderService } from '../../services/header.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  title = '';
  enableNewSearch = false;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private headerService: HeaderService
  ) {}

  ngOnInit(): void {
    this.listenHeaderChanges();
    this.route.queryParams.subscribe((p) => {
      p['enableSearch']
        ? (this.enableNewSearch = p['enableSearch'])
        : (this.enableNewSearch = false);
    });
  }

  listenHeaderChanges() {
    this.headerService.title.subscribe((title) => (this.title = title));
  }

  navigateToSearch() {
    this.router.navigate(['restaurants/search']);
  }
}
