import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class HeaderService {
  public title: BehaviorSubject<string> = new BehaviorSubject<string>('');

  constructor() {}

  updateHeaderTitle(title: string) {
    this.title.next(title);
  }
}
