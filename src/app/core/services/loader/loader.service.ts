import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class LoaderService {
  loader: Array<boolean> = [];
  isLoading = new BehaviorSubject<boolean>(false);

  constructor() {}

  show() {
    this.loader.push(true);
    this.isLoading.next(true);
  }

  hide() {
    this.loader.shift();
    if (this.loader.length === 0) this.isLoading.next(false);
  }
}
