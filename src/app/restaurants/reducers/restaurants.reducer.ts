import { createReducer, createSelector, on } from '@ngrx/store';
import { environment } from 'src/environments/environment';
import * as restaurantsActions from '../../restaurants/actions/restaurants.actions';
import { RestaurantMetaData } from '../models/searchByPostalCode';

export const restaurantsFeatureKey = 'restaurantsState';

export interface RestaurantsState {
  openRestaurants: Array<RestaurantMetaData>;
  closedRestaurants: Array<RestaurantMetaData>;
  postalCode: string;
  currentPage: number;
}

export const initialState: RestaurantsState = {
  openRestaurants: [],
  closedRestaurants: [],
  postalCode: '',
  currentPage: 0,
};

export const restaurantsReducer = createReducer(
  initialState,
  on(restaurantsActions.searchByPostalCode, (state, action) => ({
    ...state,
    postalCode: action.postalCode,
    openRestaurants: [],
    closedRestaurants: [],
  })),
  on(restaurantsActions.searchByPostalCodeComplete, (state, newPosts) => ({
    ...state,
    openRestaurants: newPosts.payload.Restaurants.filter((r) => r.IsOpenNow),
    closedRestaurants: newPosts.payload.Restaurants.filter((r) => !r.IsOpenNow),
  })),
  on(restaurantsActions.searchByPostalCodeFailed, (state) => ({ ...state })),
  on(restaurantsActions.pageNavigation, (state, action) => ({
    ...state,
    currentPage: action.pageIndex,
  }))
);

export const selectRestaurantsState = (state: any) => state.restaurantsState;

export const selectRestaurants = createSelector(
  selectRestaurantsState,
  (state: RestaurantsState) => state.openRestaurants
);

export const selectAllOpenRestaurants = createSelector(
  selectRestaurantsState,
  (state: RestaurantsState) => {
    return {
      openedRestaurants: state.openRestaurants,
      totalOpened: state.openRestaurants.length,
      currentPage: state.currentPage,
    };
  }
);

export const selectAllOpenRestaurantsPaginated = createSelector(
  selectAllOpenRestaurants,
  (restaurants) => {
    return {
      openedRestaurants: restaurants.openedRestaurants.slice(
        restaurants.currentPage === 0
          ? 0
          : restaurants.currentPage * environment.pageSize,
        (restaurants.currentPage + 1) * environment.pageSize
      ),
      totalOpened: restaurants.openedRestaurants.length,
      totalPages: Math.round(
        restaurants.openedRestaurants.length / environment.pageSize
      ),
    };
  }
);
