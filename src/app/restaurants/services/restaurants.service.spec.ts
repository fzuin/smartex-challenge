import { TestBed } from '@angular/core/testing';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { RestaurantsService } from './restaurants.service';
import { environment } from 'src/environments/environment';
import { SearchByPostalCodeResponse } from '../models/searchByPostalCode';

describe('RestaurantsService', () => {
  let service: RestaurantsService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [RestaurantsService],
    });

    service = TestBed.inject(RestaurantsService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('searchRestaurantsByPostalCode', () => {
    it('should return an Observable<SearchByPostalCodeResponse>', () => {
      const postalCode = '12345';
      const dummyResponse: SearchByPostalCodeResponse = {
        Area: 'Example Area',
        MetaData: {
          CanonicalName: 'Example Canonical Name',
          District: 'Example District',
          Postcode: '12345',
          Area: 'Example Area',
          Latitude: '37.7749',
          Longitude: '-122.4194',
        },
        Restaurants: [
          {
            Id: 158035,
            Name: 'Tops Pizza',
            UniqueName: 'tops-pizza-new-islington',
            Address: {
              City: 'London',
              FirstLine: 'Ground Floor 22 Penton Street',
              Postcode: 'N1 9PS',
              Latitude: 51.53225,
              Longitude: -0.11115,
            },
            City: 'London',
            Postcode: 'N1 9PS',
            Latitude: 0,
            Longitude: 0,
            Rating: {
              Count: 623,
              Average: 4.2,
              StarRating: 4.2,
            },
            RatingStars: 4.2,
            NumberOfRatings: 623,
            RatingAverage: 4.2,
            Description: '',
            Url: 'https://www.just-eat.co.uk/restaurants-tops-pizza-new-islington',
            LogoUrl:
              'http://d30v2pzvrfyzpo.cloudfront.net/uk/images/restaurants/158035.gif',
            IsTestRestaurant: false,
            IsHalal: false,
            IsNew: false,
            ReasonWhyTemporarilyOffline: '',
            DriveDistance: 1.4,
            DriveInfoCalculated: true,
            IsCloseBy: false,
            OfferPercent: 0,
            NewnessDate: new Date(),
            OpeningTime: new Date(),
            OpeningTimeUtc: new Date(),
            OpeningTimeIso: new Date(),
            OpeningTimeLocal: new Date(),
            DeliveryOpeningTimeLocal: new Date(),
            DeliveryOpeningTime: new Date(),
            DeliveryOpeningTimeUtc: new Date(),
            DeliveryStartTime: new Date(),
            DeliveryTime: null,
            DeliveryTimeMinutes: null,
            DeliveryWorkingTimeMinutes: 40,
            DeliveryEtaMinutes: {
              Approximate: null,
              RangeLower: 30,
              RangeUpper: 45,
            },
            IsCollection: true,
            IsDelivery: true,
            IsFreeDelivery: false,
            IsOpenNowForCollection: true,
            IsOpenNowForDelivery: true,
            IsOpenNowForPreorder: false,
            IsOpenNow: true,
            IsTemporarilyOffline: false,
            DeliveryMenuId: null,
            CollectionMenuId: null,
            DeliveryZipcode: 'EC4M',
            DeliveryCost: 1.99,
            MinimumDeliveryValue: 9.99,
            SecondDateRanking: 0,
            DefaultDisplayRank: 0,
            SponsoredPosition: 1,
            SecondDateRank: 0,
            Score: 1331,
            IsTemporaryBoost: false,
            IsSponsored: true,
            IsPremier: false,
            HygieneRating: null,
            ShowSmiley: false,
            SmileyDate: null,
            SmileyElite: false,
            SmileyResult: null,
            SmileyUrl: null,
            SendsOnItsWayNotifications: false,
            BrandName: '',
            IsBrand: false,
            LastUpdated: new Date(),
            Deals: [],
            Offers: [],
            Logo: [
              {
                StandardResolutionURL:
                  'http://d30v2pzvrfyzpo.cloudfront.net/uk/images/restaurants/158035.gif',
              },
            ],
            Tags: ['collection', 'four_star', 'open_now', 'fsa'],
            DeliveryChargeBands: [],
            CuisineTypes: [
              {
                Id: 82,
                IsTopCuisine: true,
                Name: 'Pizza',
                SeoName: 'pizza',
              },
              {
                Id: 27,
                IsTopCuisine: true,
                Name: 'Italian',
                SeoName: 'italian',
              },
            ],
            Cuisines: [
              {
                Name: 'Pizza',
                SeoName: 'pizza',
              },
              {
                Name: 'Italian',
                SeoName: 'italian',
              },
            ],
            ScoreMetaData: [
              {
                Key: 'Distance',
                Value: '1.4',
              },
              {
                Key: 'SetName',
                Value: 'Sponsored',
              },
            ],
            Badges: [],
            OpeningTimes: [],
            ServiceableAreas: [],
          },
        ],
      };

      service
        .searchRestaurantsByPostalCode(postalCode)
        .subscribe((response) => {
          expect(response).toEqual(dummyResponse);
        });

      const req = httpMock.expectOne(
        `${environment.apiUrl}restaurants/bypostcode/${postalCode}?ratingsOutOfFive=true`
      );
      expect(req.request.method).toBe('GET');
      req.flush(dummyResponse);
    });
  });

  describe('getPostalCodeByLatitudeAndLongitude', () => {
    it('should return an Observable', () => {
      const dummyPosition: GeolocationPosition = {
        coords: {
          latitude: 37.7749,
          longitude: -122.4194,
          accuracy: 1,
          altitude: 1,
          altitudeAccuracy: 1,
          heading: 0,
          speed: 0,
        },
        timestamp: 0,
      };
      const dummyResponse = {
        /* dummy response object */
      };

      service
        .getPostalCodeByLatitudeAndLongitude(dummyPosition)
        .subscribe((response) => {
          expect(response).toEqual(dummyResponse);
        });

      const req = httpMock.expectOne(
        `${environment.postalCodeAPI}points/${dummyPosition.coords.latitude},${dummyPosition.coords.longitude}`
      );
      expect(req.request.method).toBe('GET');
      req.flush(dummyResponse);
    });
  });
});
