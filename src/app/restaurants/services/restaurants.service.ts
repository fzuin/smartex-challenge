import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { SearchByPostalCodeResponse } from '../models/searchByPostalCode';

@Injectable({
  providedIn: 'root',
})
export class RestaurantsService {
  constructor(private httpClient: HttpClient) {}

  searchRestaurantsByPostalCode(postalCode: string) {
    console.log(postalCode);
    const API_ENDPOINT_POSTCODE = `restaurants/bypostcode/${postalCode}?ratingsOutOfFive=true`;
    return this.httpClient.get<SearchByPostalCodeResponse>(
      `${environment.apiUrl}${API_ENDPOINT_POSTCODE}`
    );
  }

  getPostalCodeByLatitudeAndLongitude(position: GeolocationPosition) {
    const API_ENDPOINT_GETPOSTALCODE = 'points/';
    return this.httpClient.get(
      `${environment.postalCodeAPI}${API_ENDPOINT_GETPOSTALCODE}${position.coords.latitude},${position.coords.longitude}`
    );
  }
}
