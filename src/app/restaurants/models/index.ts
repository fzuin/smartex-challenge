import { FormControl } from '@angular/forms';

export interface PostalCodeSearchForm {
  postalCode: FormControl<string>;
}
