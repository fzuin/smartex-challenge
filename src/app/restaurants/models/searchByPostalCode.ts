export interface SearchByPostalCodeResponse {
  Area: string;
  MetaData: {
    CanonicalName: string;
    District: string;
    Postcode: string;
    Area: string;
    Latitude: string;
    Longitude: string;
  };
  Restaurants: Array<RestaurantMetaData>;
}

export interface Address {
  City: string;
  FirstLine: string;
  Postcode: string;
  Latitude: number;
  Longitude: number;
}

export interface Rating {
  Count: number;
  Average: number;
  StarRating: number;
}

export interface DeliveryEtaMinutes {
  Approximate?: any;
  RangeLower: number;
  RangeUpper: number;
}

export interface Logo {
  StandardResolutionURL: string;
}

export interface CuisineType {
  Id: number;
  IsTopCuisine: boolean;
  Name: string;
  SeoName: string;
}

export interface Cuisine {
  Name: string;
  SeoName: string;
}

export interface ScoreMetaData {
  Key: string;
  Value: string;
}

export interface RestaurantMetaData {
  Id: number;
  Name: string;
  UniqueName: string;
  Address: Address;
  City: string;
  Postcode: string;
  Latitude: number;
  Longitude: number;
  Rating: Rating;
  RatingStars: number;
  NumberOfRatings: number;
  RatingAverage: number;
  Description: string;
  Url: string;
  LogoUrl: string;
  IsTestRestaurant: boolean;
  IsHalal: boolean;
  IsNew: boolean;
  ReasonWhyTemporarilyOffline: string;
  DriveDistance: number;
  DriveInfoCalculated: boolean;
  IsCloseBy: boolean;
  OfferPercent: number;
  NewnessDate: Date;
  OpeningTime: Date;
  OpeningTimeUtc: Date;
  OpeningTimeIso: Date;
  OpeningTimeLocal: Date;
  DeliveryOpeningTimeLocal: Date;
  DeliveryOpeningTime: Date;
  DeliveryOpeningTimeUtc: Date;
  DeliveryStartTime: Date;
  DeliveryTime?: any;
  DeliveryTimeMinutes?: any;
  DeliveryWorkingTimeMinutes: number;
  DeliveryEtaMinutes: DeliveryEtaMinutes;
  IsCollection: boolean;
  IsDelivery: boolean;
  IsFreeDelivery: boolean;
  IsOpenNowForCollection: boolean;
  IsOpenNowForDelivery: boolean;
  IsOpenNowForPreorder: boolean;
  IsOpenNow: boolean;
  IsTemporarilyOffline: boolean;
  DeliveryMenuId?: any;
  CollectionMenuId?: any;
  DeliveryZipcode: string;
  DeliveryCost: number;
  MinimumDeliveryValue: number;
  SecondDateRanking: number;
  DefaultDisplayRank: number;
  SponsoredPosition: number;
  SecondDateRank: number;
  Score: number;
  IsTemporaryBoost: boolean;
  IsSponsored: boolean;
  IsPremier: boolean;
  HygieneRating?: any;
  ShowSmiley: boolean;
  SmileyDate?: any;
  SmileyElite: boolean;
  SmileyResult?: any;
  SmileyUrl?: any;
  SendsOnItsWayNotifications: boolean;
  BrandName: string;
  IsBrand: boolean;
  LastUpdated: Date;
  Deals: any[];
  Offers: any[];
  Logo: Logo[];
  Tags: string[];
  DeliveryChargeBands: any[];
  CuisineTypes: CuisineType[];
  Cuisines: Cuisine[];
  ScoreMetaData: ScoreMetaData[];
  Badges: any[];
  OpeningTimes: any[];
  ServiceableAreas: any[];
}
