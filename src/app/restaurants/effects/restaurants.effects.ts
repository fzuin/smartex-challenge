import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { mergeMap, map, catchError, of } from 'rxjs';
import {
  searchByPostalCode,
  SEARCH_BY_POSTALCODE_COMPLETE,
  SEARCH_BY_POSTALCODE_FAILED,
} from '../actions/restaurants.actions';
import { RestaurantsService } from '../services/restaurants.service';

@Injectable()
export class RestaurantsEffects {
  searchRestaurantsByPostalCode$ = createEffect(() =>
    this.actions$.pipe(
      ofType(searchByPostalCode),
      mergeMap((action) =>
        this.restaurantsService
          .searchRestaurantsByPostalCode(action.postalCode)
          .pipe(
            map((restaurantsMetaData) => {
              return {
                type: SEARCH_BY_POSTALCODE_COMPLETE,
                payload: restaurantsMetaData,
              };
            }),
            catchError(() => of({ type: SEARCH_BY_POSTALCODE_FAILED }))
          )
      )
    )
  );

  constructor(
    private actions$: Actions,
    private restaurantsService: RestaurantsService
  ) {}
}
