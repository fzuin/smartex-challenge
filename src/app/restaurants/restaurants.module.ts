import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import * as fromRestaurants from './reducers/restaurants.reducer';
import { ListComponent } from './pages/list/list.component';

import { RestaurantsRoutingModule } from './restaurants-routing.module';
import { RestaurantsEffects } from './effects/restaurants.effects';
import { RestaurantCardComponent } from './components/restaurant-card/restaurant-card.component';
import { RatingStarComponent } from './components/rating-star/rating-star.component';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { MatChipsModule } from '@angular/material/chips';
import { FoodTypesChipComponent } from './components/food-types-chip/food-types-chip.component';
import { SearchComponent } from './pages/search/search.component';
import { MatInputModule } from '@angular/material/input';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PaginationComponent } from './components/pagination/pagination.component';
import { MatPaginatorModule } from '@angular/material/paginator';
@NgModule({
  declarations: [
    ListComponent,
    RestaurantCardComponent,
    RatingStarComponent,
    FoodTypesChipComponent,
    SearchComponent,
    PaginationComponent,
  ],
  imports: [
    RestaurantsRoutingModule,
    CommonModule,
    MatCardModule,
    MatChipsModule,
    MatInputModule,
    MatButtonModule,
    ReactiveFormsModule,
    FormsModule,
    MatSnackBarModule,
    MatPaginatorModule,
    StoreModule.forFeature(
      fromRestaurants.restaurantsFeatureKey,
      fromRestaurants.restaurantsReducer
    ),
    EffectsModule.forFeature([RestaurantsEffects]),
  ],
})
export class RestaurantsModule {}
