import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CuisineType } from '../../models/searchByPostalCode';
import { FoodTypesChipComponent } from './food-types-chip.component';

describe('FoodTypesChipComponent', () => {
  let component: FoodTypesChipComponent;
  let fixture: ComponentFixture<FoodTypesChipComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [FoodTypesChipComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FoodTypesChipComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  /*
  it('should display cuisine types chips', () => {
    const cuisineTypes: CuisineType[] = [
      { Id: 1, IsTopCuisine: true, Name: 'Chinese', SeoName: 'chinese' },
      { Id: 2, IsTopCuisine: true, Name: 'Indian', SeoName: 'indian' },
    ];

    component.cuisineTypes = cuisineTypes;
    fixture.detectChanges();

    const chips = fixture.nativeElement.querySelectorAll('.mat-chip');

    for (let i = 0; i < cuisineTypes.length; i++) {
      const chipText = chips[i].textContent.trim();
      expect(chipText).toBe(cuisineTypes[i].Name);
    }
  });*/
});
