import { Component, Input, OnInit } from '@angular/core';
import { CuisineType } from '../../models/searchByPostalCode';

@Component({
  selector: 'app-food-types-chip',
  templateUrl: './food-types-chip.component.html',
  styleUrls: ['./food-types-chip.component.scss'],
})
export class FoodTypesChipComponent {
  @Input() cuisineTypes: Array<CuisineType> = [];

  constructor() {}
}
