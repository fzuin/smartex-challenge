import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-rating-star',
  templateUrl: './rating-star.component.html',
  styleUrls: ['./rating-star.component.scss'],
})
export class RatingStarComponent implements OnInit {
  @Input() rating: number = 1;
  stars: Array<number> = [];

  constructor() {}

  ngOnInit(): void {
    try {
      this.stars = Array(Math.round(this.rating))
        .fill(0)
        .map((x, i) => i);
    } catch (error) {
      this.stars = [0];
    }
  }
}
