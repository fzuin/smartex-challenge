import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RatingStarComponent } from './rating-star.component';

describe('RatingStarComponent', () => {
  let component: RatingStarComponent;
  let fixture: ComponentFixture<RatingStarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [RatingStarComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RatingStarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should create an array of stars based on the rating input', () => {
    component.rating = 3.5;
    component.ngOnInit();
    expect(component.stars.length).toBe(4);
  });

  it('should create an array of one star when the rating input is undefined', () => {
    component.rating = 0.6;
    component.ngOnInit();
    expect(component.stars.length).toBe(1);
  });
});
