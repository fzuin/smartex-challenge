import { Component, EventEmitter, Input, Output } from '@angular/core';
import { PageEvent } from '@angular/material/paginator';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss'],
})
export class PaginationComponent {
  @Input() length = 0;
  @Output() paginationEvent = new EventEmitter();
  pageSize = environment.pageSize;

  handlePageEvent(event: PageEvent) {
    this.paginationEvent.emit(event);
  }
}
