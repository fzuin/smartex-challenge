import { Component, Input, OnInit } from '@angular/core';
import { fadeAnimation } from 'src/app/core/utils/animations';
import { RestaurantMetaData } from '../../models/searchByPostalCode';

@Component({
  selector: 'app-restaurant-card',
  templateUrl: './restaurant-card.component.html',
  animations: [fadeAnimation],
  styleUrls: ['./restaurant-card.component.scss'],
})
export class RestaurantCardComponent implements OnInit {
  @Input() restaurantMetaData!: RestaurantMetaData;

  constructor() {}

  ngOnInit(): void {}
}
