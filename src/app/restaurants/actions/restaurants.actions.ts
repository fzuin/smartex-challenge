import { createAction, props } from '@ngrx/store';
import { SearchByPostalCodeResponse } from '../models/searchByPostalCode';

export const SEARCH_BY_POSTALCODE = '[RESTAURANTS] Search by postal code';
export const SEARCH_BY_POSTALCODE_COMPLETE =
  '[RESTAURANTS] Search by postal code Completed';
export const SEARCH_BY_POSTALCODE_FAILED =
  '[RESTAURANTS] Search by postal code Failed';

export const PAGE_NAVIGATION = '[RESTAURANTS] Page navigation Event';

export const searchByPostalCode = createAction(
  SEARCH_BY_POSTALCODE,
  props<{ postalCode: string }>()
);

export const searchByPostalCodeComplete = createAction(
  SEARCH_BY_POSTALCODE_COMPLETE,
  props<{ payload: SearchByPostalCodeResponse }>()
);

export const searchByPostalCodeFailed = createAction(
  SEARCH_BY_POSTALCODE_FAILED,
  props<any>()
);

export const pageNavigation = createAction(
  PAGE_NAVIGATION,
  props<{ pageIndex: number }>()
);
