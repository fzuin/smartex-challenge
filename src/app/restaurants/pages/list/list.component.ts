import {
  Component,
  ElementRef,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { PageEvent } from '@angular/material/paginator';
import { select, Store } from '@ngrx/store';
import { filter, Subject, takeUntil } from 'rxjs';
import { HeaderService } from 'src/app/core/services/header.service';
import { LoaderService } from 'src/app/core/services/loader/loader.service';
import { fadeAnimation } from 'src/app/core/utils/animations';
import {
  pageNavigation,
  searchByPostalCode,
} from '../../actions/restaurants.actions';
import { RestaurantMetaData } from '../../models/searchByPostalCode';
import { selectAllOpenRestaurantsPaginated } from '../../reducers/restaurants.reducer';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  animations: [fadeAnimation],
  styleUrls: ['./list.component.scss'],
})
export class ListComponent implements OnInit, OnDestroy {
  @ViewChild('container') container!: ElementRef;
  isLoading = this.loaderService.isLoading;
  restaurants: Array<RestaurantMetaData> = [];
  totalRestaurants = 0;
  postalCode = '';
  private readonly destroying$ = new Subject<void>();

  constructor(
    private readonly store: Store,
    private headerService: HeaderService,
    private route: ActivatedRoute,
    private loaderService: LoaderService
  ) {}

  ngOnInit(): void {
    this.postalCode = this.route.snapshot.paramMap.get('postalCode') || '';
    this.headerService.updateHeaderTitle(
      'Restaurants near of ' + this.postalCode
    );
    this.store.dispatch(searchByPostalCode({ postalCode: this.postalCode }));
    this.listenForRestaurantsState();
  }

  ngOnDestroy(): void {
    this.destroying$.next(undefined);
    this.destroying$.complete();
  }

  listenForRestaurantsState() {
    this.store
      .pipe(
        select(selectAllOpenRestaurantsPaginated),
        takeUntil(this.destroying$),
        filter(
          (val) =>
            val && val.openedRestaurants && val.openedRestaurants.length !== 0
        )
      )
      .subscribe({
        next: (restaurantData) => {
          this.restaurants = restaurantData.openedRestaurants;
          this.totalRestaurants = restaurantData.totalOpened;
          this.resetScroll();
        },
      });
  }

  handlePagination(event: PageEvent) {
    this.store.dispatch(pageNavigation({ pageIndex: event.pageIndex }));
  }

  resetScroll() {
    this.container.nativeElement.scrollTo(0, 0);
  }
}
