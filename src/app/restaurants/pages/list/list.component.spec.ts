import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { provideMockStore, MockStore } from '@ngrx/store/testing';
import { filter } from 'rxjs/operators';
import { HeaderService } from 'src/app/core/services/header.service';
import { RestaurantMetaData } from '../../models/searchByPostalCode';
import { searchByPostalCode } from '../../actions/restaurants.actions';
import {
  RestaurantsState,
  selectAllOpenRestaurantsPaginated,
} from '../../reducers/restaurants.reducer';
import { ListComponent } from './list.component';
import { RestaurantsModule } from '../../restaurants.module';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { HttpClientModule } from '@angular/common/http';
import { of } from 'rxjs';

describe('ListComponent', () => {
  let component: ListComponent;
  let fixture: ComponentFixture<ListComponent>;
  let store: MockStore;
  let headerService: jasmine.SpyObj<HeaderService>;

  const initialState: RestaurantsState = {
    openRestaurants: [
      {
        Id: 158035,
        Name: 'Tops Pizza',
        UniqueName: 'tops-pizza-new-islington',
        Address: {
          City: 'London',
          FirstLine: 'Ground Floor 22 Penton Street',
          Postcode: 'N1 9PS',
          Latitude: 51.53225,
          Longitude: -0.11115,
        },
        City: 'London',
        Postcode: 'N1 9PS',
        Latitude: 0,
        Longitude: 0,
        Rating: {
          Count: 623,
          Average: 4.2,
          StarRating: 4.2,
        },
        RatingStars: 4.2,
        NumberOfRatings: 623,
        RatingAverage: 4.2,
        Description: '',
        Url: 'https://www.just-eat.co.uk/restaurants-tops-pizza-new-islington',
        LogoUrl:
          'http://d30v2pzvrfyzpo.cloudfront.net/uk/images/restaurants/158035.gif',
        IsTestRestaurant: false,
        IsHalal: false,
        IsNew: false,
        ReasonWhyTemporarilyOffline: '',
        DriveDistance: 1.4,
        DriveInfoCalculated: true,
        IsCloseBy: false,
        OfferPercent: 0,
        NewnessDate: new Date(),
        OpeningTime: new Date(),
        OpeningTimeUtc: new Date(),
        OpeningTimeIso: new Date(),
        OpeningTimeLocal: new Date(),
        DeliveryOpeningTimeLocal: new Date(),
        DeliveryOpeningTime: new Date(),
        DeliveryOpeningTimeUtc: new Date(),
        DeliveryStartTime: new Date(),
        DeliveryTime: null,
        DeliveryTimeMinutes: null,
        DeliveryWorkingTimeMinutes: 40,
        DeliveryEtaMinutes: {
          Approximate: null,
          RangeLower: 30,
          RangeUpper: 45,
        },
        IsCollection: true,
        IsDelivery: true,
        IsFreeDelivery: false,
        IsOpenNowForCollection: true,
        IsOpenNowForDelivery: true,
        IsOpenNowForPreorder: false,
        IsOpenNow: true,
        IsTemporarilyOffline: false,
        DeliveryMenuId: null,
        CollectionMenuId: null,
        DeliveryZipcode: 'EC4M',
        DeliveryCost: 1.99,
        MinimumDeliveryValue: 9.99,
        SecondDateRanking: 0,
        DefaultDisplayRank: 0,
        SponsoredPosition: 1,
        SecondDateRank: 0,
        Score: 1331,
        IsTemporaryBoost: false,
        IsSponsored: true,
        IsPremier: false,
        HygieneRating: null,
        ShowSmiley: false,
        SmileyDate: null,
        SmileyElite: false,
        SmileyResult: null,
        SmileyUrl: null,
        SendsOnItsWayNotifications: false,
        BrandName: '',
        IsBrand: false,
        LastUpdated: new Date(),
        Deals: [],
        Offers: [],
        Logo: [
          {
            StandardResolutionURL:
              'http://d30v2pzvrfyzpo.cloudfront.net/uk/images/restaurants/158035.gif',
          },
        ],
        Tags: ['collection', 'four_star', 'open_now', 'fsa'],
        DeliveryChargeBands: [],
        CuisineTypes: [
          {
            Id: 82,
            IsTopCuisine: true,
            Name: 'Pizza',
            SeoName: 'pizza',
          },
          {
            Id: 27,
            IsTopCuisine: true,
            Name: 'Italian',
            SeoName: 'italian',
          },
        ],
        Cuisines: [
          {
            Name: 'Pizza',
            SeoName: 'pizza',
          },
          {
            Name: 'Italian',
            SeoName: 'italian',
          },
        ],
        ScoreMetaData: [
          {
            Key: 'Distance',
            Value: '1.4',
          },
          {
            Key: 'SetName',
            Value: 'Sponsored',
          },
        ],
        Badges: [],
        OpeningTimes: [],
        ServiceableAreas: [],
      },
    ],
    closedRestaurants: [],
    postalCode: '',
    currentPage: 0,
  };

  const mockActivatedRoute = {
    snapshot: {
      paramMap: {
        get: jasmine.createSpy().and.returnValue('SW1A'),
      },
    },
  };

  beforeEach(async () => {
    const headerServiceSpy = jasmine.createSpyObj('HeaderService', [
      'updateHeaderTitle',
    ]);

    await TestBed.configureTestingModule({
      declarations: [ListComponent],
      providers: [
        provideMockStore({
          initialState,
          selectors: [
            {
              selector: selectAllOpenRestaurantsPaginated,
              value: [
                {
                  openedRestaurants: [
                    {
                      Id: 158035,
                      Name: 'Tops Pizza',
                      UniqueName: 'tops-pizza-new-islington',
                      Address: {
                        City: 'London',
                        FirstLine: 'Ground Floor 22 Penton Street',
                        Postcode: 'N1 9PS',
                        Latitude: 51.53225,
                        Longitude: -0.11115,
                      },
                      City: 'London',
                      Postcode: 'N1 9PS',
                      Latitude: 0,
                      Longitude: 0,
                      Rating: {
                        Count: 623,
                        Average: 4.2,
                        StarRating: 4.2,
                      },
                      RatingStars: 4.2,
                      NumberOfRatings: 623,
                      RatingAverage: 4.2,
                      Description: '',
                      Url: 'https://www.just-eat.co.uk/restaurants-tops-pizza-new-islington',
                      LogoUrl:
                        'http://d30v2pzvrfyzpo.cloudfront.net/uk/images/restaurants/158035.gif',
                      IsTestRestaurant: false,
                      IsHalal: false,
                      IsNew: false,
                      ReasonWhyTemporarilyOffline: '',
                      DriveDistance: 1.4,
                      DriveInfoCalculated: true,
                      IsCloseBy: false,
                      OfferPercent: 0,
                      NewnessDate: new Date(),
                      OpeningTime: new Date(),
                      OpeningTimeUtc: new Date(),
                      OpeningTimeIso: new Date(),
                      OpeningTimeLocal: new Date(),
                      DeliveryOpeningTimeLocal: new Date(),
                      DeliveryOpeningTime: new Date(),
                      DeliveryOpeningTimeUtc: new Date(),
                      DeliveryStartTime: new Date(),
                      DeliveryTime: null,
                      DeliveryTimeMinutes: null,
                      DeliveryWorkingTimeMinutes: 40,
                      DeliveryEtaMinutes: {
                        Approximate: null,
                        RangeLower: 30,
                        RangeUpper: 45,
                      },
                      IsCollection: true,
                      IsDelivery: true,
                      IsFreeDelivery: false,
                      IsOpenNowForCollection: true,
                      IsOpenNowForDelivery: true,
                      IsOpenNowForPreorder: false,
                      IsOpenNow: true,
                      IsTemporarilyOffline: false,
                      DeliveryMenuId: null,
                      CollectionMenuId: null,
                      DeliveryZipcode: 'EC4M',
                      DeliveryCost: 1.99,
                      MinimumDeliveryValue: 9.99,
                      SecondDateRanking: 0,
                      DefaultDisplayRank: 0,
                      SponsoredPosition: 1,
                      SecondDateRank: 0,
                      Score: 1331,
                      IsTemporaryBoost: false,
                      IsSponsored: true,
                      IsPremier: false,
                      HygieneRating: null,
                      ShowSmiley: false,
                      SmileyDate: null,
                      SmileyElite: false,
                      SmileyResult: null,
                      SmileyUrl: null,
                      SendsOnItsWayNotifications: false,
                      BrandName: '',
                      IsBrand: false,
                      LastUpdated: new Date(),
                      Deals: [],
                      Offers: [],
                      Logo: [
                        {
                          StandardResolutionURL:
                            'http://d30v2pzvrfyzpo.cloudfront.net/uk/images/restaurants/158035.gif',
                        },
                      ],
                      Tags: ['collection', 'four_star', 'open_now', 'fsa'],
                      DeliveryChargeBands: [],
                      CuisineTypes: [
                        {
                          Id: 82,
                          IsTopCuisine: true,
                          Name: 'Pizza',
                          SeoName: 'pizza',
                        },
                        {
                          Id: 27,
                          IsTopCuisine: true,
                          Name: 'Italian',
                          SeoName: 'italian',
                        },
                      ],
                      Cuisines: [
                        {
                          Name: 'Pizza',
                          SeoName: 'pizza',
                        },
                        {
                          Name: 'Italian',
                          SeoName: 'italian',
                        },
                      ],
                      ScoreMetaData: [
                        {
                          Key: 'Distance',
                          Value: '1.4',
                        },
                        {
                          Key: 'SetName',
                          Value: 'Sponsored',
                        },
                      ],
                      Badges: [],
                      OpeningTimes: [],
                      ServiceableAreas: [],
                    },
                  ],
                  totalOpened: 1,
                  currentPage: 1,
                },
              ],
            },
          ],
        }),
        { provide: HeaderService, useValue: headerServiceSpy },
        { provide: ActivatedRoute, useValue: mockActivatedRoute },
      ],
      imports: [
        HttpClientModule,
        RestaurantsModule,
        StoreModule.forRoot({}),
        EffectsModule.forRoot([]),
      ],
    }).compileComponents();

    store = TestBed.inject(MockStore);
    headerService = TestBed.inject(
      HeaderService
    ) as jasmine.SpyObj<HeaderService>;

    fixture = TestBed.createComponent(ListComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('ngOnInit', () => {
    it('should call updateHeaderTitle with the correct title', () => {
      const openedRestaurants: Array<RestaurantMetaData> = [
        {
          Id: 158035,
          Name: 'Tops Pizza',
          UniqueName: 'tops-pizza-new-islington',
          Address: {
            City: 'London',
            FirstLine: 'Ground Floor 22 Penton Street',
            Postcode: 'N1 9PS',
            Latitude: 51.53225,
            Longitude: -0.11115,
          },
          City: 'London',
          Postcode: 'N1 9PS',
          Latitude: 0,
          Longitude: 0,
          Rating: {
            Count: 623,
            Average: 4.2,
            StarRating: 4.2,
          },
          RatingStars: 4.2,
          NumberOfRatings: 623,
          RatingAverage: 4.2,
          Description: '',
          Url: 'https://www.just-eat.co.uk/restaurants-tops-pizza-new-islington',
          LogoUrl:
            'http://d30v2pzvrfyzpo.cloudfront.net/uk/images/restaurants/158035.gif',
          IsTestRestaurant: false,
          IsHalal: false,
          IsNew: false,
          ReasonWhyTemporarilyOffline: '',
          DriveDistance: 1.4,
          DriveInfoCalculated: true,
          IsCloseBy: false,
          OfferPercent: 0,
          NewnessDate: new Date(),
          OpeningTime: new Date(),
          OpeningTimeUtc: new Date(),
          OpeningTimeIso: new Date(),
          OpeningTimeLocal: new Date(),
          DeliveryOpeningTimeLocal: new Date(),
          DeliveryOpeningTime: new Date(),
          DeliveryOpeningTimeUtc: new Date(),
          DeliveryStartTime: new Date(),
          DeliveryTime: null,
          DeliveryTimeMinutes: null,
          DeliveryWorkingTimeMinutes: 40,
          DeliveryEtaMinutes: {
            Approximate: null,
            RangeLower: 30,
            RangeUpper: 45,
          },
          IsCollection: true,
          IsDelivery: true,
          IsFreeDelivery: false,
          IsOpenNowForCollection: true,
          IsOpenNowForDelivery: true,
          IsOpenNowForPreorder: false,
          IsOpenNow: true,
          IsTemporarilyOffline: false,
          DeliveryMenuId: null,
          CollectionMenuId: null,
          DeliveryZipcode: 'EC4M',
          DeliveryCost: 1.99,
          MinimumDeliveryValue: 9.99,
          SecondDateRanking: 0,
          DefaultDisplayRank: 0,
          SponsoredPosition: 1,
          SecondDateRank: 0,
          Score: 1331,
          IsTemporaryBoost: false,
          IsSponsored: true,
          IsPremier: false,
          HygieneRating: null,
          ShowSmiley: false,
          SmileyDate: null,
          SmileyElite: false,
          SmileyResult: null,
          SmileyUrl: null,
          SendsOnItsWayNotifications: false,
          BrandName: '',
          IsBrand: false,
          LastUpdated: new Date(),
          Deals: [],
          Offers: [],
          Logo: [
            {
              StandardResolutionURL:
                'http://d30v2pzvrfyzpo.cloudfront.net/uk/images/restaurants/158035.gif',
            },
          ],
          Tags: ['collection', 'four_star', 'open_now', 'fsa'],
          DeliveryChargeBands: [],
          CuisineTypes: [
            {
              Id: 82,
              IsTopCuisine: true,
              Name: 'Pizza',
              SeoName: 'pizza',
            },
            {
              Id: 27,
              IsTopCuisine: true,
              Name: 'Italian',
              SeoName: 'italian',
            },
          ],
          Cuisines: [
            {
              Name: 'Pizza',
              SeoName: 'pizza',
            },
            {
              Name: 'Italian',
              SeoName: 'italian',
            },
          ],
          ScoreMetaData: [
            {
              Key: 'Distance',
              Value: '1.4',
            },
            {
              Key: 'SetName',
              Value: 'Sponsored',
            },
          ],
          Badges: [],
          OpeningTimes: [],
          ServiceableAreas: [],
        },
      ];

      const totalOpened = 1;

      store.setState({
        ...initialState,
        openedRestaurants,
        totalOpened,
      });

      component.ngOnInit();
      expect(headerService.updateHeaderTitle).toHaveBeenCalledWith(
        'Restaurants near of SW1A'
      );
    });

    it('should dispatch the searchByPostalCode action with the correct payload', () => {
      spyOn(store, 'dispatch').and.callThrough();

      component.ngOnInit();
      expect(store.dispatch).toHaveBeenCalledWith(
        searchByPostalCode({ postalCode: 'SW1A' })
      );
    });

    it('should call listenForRestaurantsState', () => {
      spyOn(component, 'listenForRestaurantsState').and.callThrough();

      component.ngOnInit();
      expect(component.listenForRestaurantsState).toHaveBeenCalled();
    });
  });
});
