import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { HeaderService } from 'src/app/core/services/header.service';
import { RestaurantsService } from '../../services/restaurants.service';

import { SearchComponent } from './search.component';

describe('SearchComponent', () => {
  let component: SearchComponent;
  let fixture: ComponentFixture<SearchComponent>;
  let headerServiceSpy: jasmine.SpyObj<HeaderService>;
  let restaurantServiceSpy: jasmine.SpyObj<RestaurantsService>;
  let snackBarSpy: jasmine.SpyObj<MatSnackBar>;
  let routerSpy: jasmine.SpyObj<Router>;

  beforeEach(async () => {
    const headerService = jasmine.createSpyObj('HeaderService', [
      'updateHeaderTitle',
    ]);
    const restaurantService = jasmine.createSpyObj('RestaurantsService', [
      'getPostalCodeByLatitudeAndLongitude',
    ]);
    const snackBar = jasmine.createSpyObj('MatSnackBar', ['open']);
    const router = jasmine.createSpyObj('Router', ['navigate']);

    await TestBed.configureTestingModule({
      declarations: [SearchComponent],
      imports: [ReactiveFormsModule],
      providers: [
        { provide: HeaderService, useValue: headerService },
        { provide: RestaurantsService, useValue: restaurantService },
        { provide: MatSnackBar, useValue: snackBar },
        { provide: Router, useValue: router },
      ],
    }).compileComponents();

    headerServiceSpy = TestBed.inject(
      HeaderService
    ) as jasmine.SpyObj<HeaderService>;
    restaurantServiceSpy = TestBed.inject(
      RestaurantsService
    ) as jasmine.SpyObj<RestaurantsService>;
    snackBarSpy = TestBed.inject(MatSnackBar) as jasmine.SpyObj<MatSnackBar>;
    routerSpy = TestBed.inject(Router) as jasmine.SpyObj<Router>;
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set header title on init', () => {
    expect(headerServiceSpy.updateHeaderTitle).toHaveBeenCalledWith(
      'Search by postal code'
    );
  });

  describe('submit', () => {
    it('should navigate to restaurant list with formatted postal code and search query param', () => {
      component.searchForm.controls.postalCode.setValue('A1B 2C3');
      component.submit();
      expect(routerSpy.navigate).toHaveBeenCalledWith(
        ['restaurants/list/A1B2C3'],
        { queryParams: { enableSearch: true } }
      );
    });
  });

  describe('useUserLocation', () => {
    let getCurrentPositionSpy: jasmine.Spy;

    beforeEach(() => {
      getCurrentPositionSpy = spyOn(
        navigator.geolocation,
        'getCurrentPosition'
      );
    });

    it('should call getCurrentPosition with options and success callback', () => {
      const options = {
        enableHighAccuracy: true,
        timeout: 5000,
        maximumAge: 0,
      };
      getCurrentPositionSpy.and.callThrough();

      component.useUserLocation();

      expect(navigator.geolocation.getCurrentPosition).toHaveBeenCalledWith(
        component['onUserLocationSuccess'],
        component['onUserLocationError'],
        options
      );
    });
  });
});
