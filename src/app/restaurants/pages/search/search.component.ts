import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Subject, takeUntil } from 'rxjs';
import { HeaderService } from 'src/app/core/services/header.service';
import { PostalCodeSearchForm } from '../../models';
import { RestaurantsService } from '../../services/restaurants.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss'],
})
export class SearchComponent implements OnInit, OnDestroy {
  private readonly destroying$ = new Subject<void>();
  private readonly SNACKBAR_TIMEOUT = 4000;

  constructor(
    private headerService: HeaderService,
    private restaurantService: RestaurantsService,
    private snackBar: MatSnackBar,
    private formBuilder: FormBuilder,
    private router: Router
  ) {}

  searchForm = this.formBuilder.group<PostalCodeSearchForm>({
    postalCode: this.formBuilder.nonNullable.control('', Validators.required),
  });

  ngOnInit(): void {
    this.headerService.updateHeaderTitle('Search by postal code');
  }

  ngOnDestroy(): void {
    this.destroying$.next(undefined);
    this.destroying$.complete();
  }

  /**
   * Navigate to restaurants list and remove the spaces before adding to the URL
   */
  submit() {
    this.router.navigate(
      [
        `restaurants/list/${this.searchForm.controls.postalCode.value.replace(
          / /g,
          ''
        )}`,
      ],
      { queryParams: { enableSearch: true } }
    );
  }

  useUserLocation() {
    const options = {
      enableHighAccuracy: true,
      timeout: 5000,
      maximumAge: 0,
    };

    navigator.geolocation.getCurrentPosition(
      this.onUserLocationSuccess,
      this.onUserLocationError,
      options
    );
  }

  private onUserLocationSuccess = (position: GeolocationPosition) => {
    this.retrieveUserPostalcodeInUK(position);
  };

  private onUserLocationError = (positionError: GeolocationPositionError) => {
    this.snackBar.open(
      'Error retrieving your location, please be sure that you accepted the location request'
    );
  };

  /**
   * I dont think it make sense to be on a reducer/side effect, so I will place it here
   * @param position
   */
  private retrieveUserPostalcodeInUK = (position: GeolocationPosition) => {
    this.restaurantService
      .getPostalCodeByLatitudeAndLongitude(position)
      .pipe(takeUntil(this.destroying$))
      .subscribe({
        next: (res) => {
          //#TODO implement a logic to get UK locations and postalcode
          this.searchForm.controls.postalCode.setValue('ec4m');
        },
        error: (err) => {
          console.log(err);
          if (err.status === 400) {
            return this.handleError400OnPostalCode();
          }
          this.snackBar.open('Something went wrong try again', 'Ok!', {
            duration: this.SNACKBAR_TIMEOUT,
          });
        },
      });
  };

  handleError400OnPostalCode() {
    this.snackBar.open(
      "Sorry, you're not in the United Kingdom, using default value",
      'Ok!',
      { duration: this.SNACKBAR_TIMEOUT }
    );
    this.searchForm.controls.postalCode.setValue('ec4m');
  }
}
